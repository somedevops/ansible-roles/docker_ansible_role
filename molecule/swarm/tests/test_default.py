"""Role testing files using testinfra."""
import pytest
import os
import testinfra.utils.ansible_runner

pytest.testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ["MOLECULE_INVENTORY_FILE"]
        )
managers = pytest.testinfra_hosts.get_hosts('managers')
workers = pytest.testinfra_hosts.get_hosts('workers')

def test_docker_swarm(host):
    docker_info = host.check_output('docker info')
    hostname = host.check_output('hostname')
    if hostname in managers:
        assert 'Swarm: active' in docker_info
    elif hostname in workers:
        assert 'Is Manager: false' in docker_info

def test_swarm_labels(host):
    hostname = host.check_output('hostname')
    if hostname in managers:
        swarm_labels = host.check_output('docker node inspect worker1-ubuntu-2004 | grep -C 2 Labels')
        assert 'foo' in swarm_labels

def test_count_managers_and_nodes(host):
    hostname = host.check_output('hostname')
    docker_info = host.check_output('docker info')
    if hostname in managers:
        assert 'Managers: 3' in docker_info
        assert 'Nodes: 5' in docker_info